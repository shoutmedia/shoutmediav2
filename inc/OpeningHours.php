<?php
/**
 * Opening Hours
 */
class OpeningHours
{
    /**
     * Return Schema openingHours
     * @param  array $opts Rendering options
     * @return void
     */
    public static function render($opts = [])
    {
        $options = [
            'display' => 'div',
            'closed' => true,
            'combine' => false,
            'format' => 'g:i a',
            'divider' => ' - ',
            'class' => 'hours',
            'between' => ': ',
            'length' => null,
        ];

        $options = array_merge($options, $opts);
        $hours = StoreHours::data();

        if ($options['combine'] == 'all') {
            $hours = StoreHours::combineAll($hours);
        }

        if ($options['combine'] == 'linear') {
            $hours = StoreHours::combineLinear($hours);
        }

        if ($options['display'] == 'table') {
            $options['between'] = '</td><td>';
        }

        $html = self::startHours($options);

        foreach ($hours as $data) {
            $labels = $data['label'];
            $lbl = $data['label'];

            if (is_array($labels)) {
                $labels = array_map(function ($label) use ($options) {
                    return is_null($options['length']) ? $label : substr($label, 0, $options['length']);
                }, $labels);

                $lbl = array_map(function ($label) {
                    return substr($label, 0, 2);
                }, $lbl);

                $lbl = implode(',', $lbl);
            } else {
                $labels = is_null($options['length']) ? $labels : substr($labels, 0, $options['length']);
                $lbl = substr($lbl, 0, 2);
            }

            if ($options['combine'] == 'all') {
                $labels = implode(', ', $labels);
            }

            if ($options['combine'] == 'linear') {
                $labels = count($labels) > 1 ? $labels[0] . '-' . $labels[count($labels) - 1] : implode(', ', $labels);
            }

            if ($data['closed']) {
                if ($options['closed']) {
                    $html .= self::beforeHour($options) . '>' . $labels . $options['between'] . __('Closed', 'schema') . self::afterHour($options);
                }
            } else {
                $html .= self::beforeHour($options) . ' itemprop="openingHours" content="' . $lbl . ' ' . $data['open'] . '-' . $data['close'] . '">' . $labels . $options['between'] . date($options['format'], strtotime($data['open'])) . $options['divider'] . date($options['format'], strtotime($data['close'])) . self::afterHour($options);
            }
        }

        $html .= self::endHours($options);

        echo $html;
    }

    /**
     * HTML to be displayed before individual entry
     * @param  array  $options Display options
     * @return string          HTML
     */
    private static function beforeHour($options)
    {
        switch ($options['display']) {
            case 'list':
                return '<li';
                break;
            case 'table':
                return '<tr><td';
                break;
            case 'div':
                return '<div';
                break;
        }
    }

    /**
     * HTML to be displayed after individual entry
     * @param  array  $options Display options
     * @return string          HTML
     */
    private static function afterHour($options)
    {
        switch ($options['display']) {
            case 'list':
                return '</li>';
                break;
            case 'table':
                return '</td></tr>';
                break;
            case 'div':
                return '</div>';
                break;
        }
    }

    /**
     * HTML to be displayed before hours output
     * @param  array  $options Display options
     * @return string          HTML
     */
    private static function startHours($options)
    {
        switch ($options['display']) {
            case 'list':
                return '<ul class="' . $options['class'] . '">';
                break;
            case 'table':
                return '<table class="' . $options['class'] . '">';
                break;
            case 'div':
                return '<div class="' . $options['class'] . '">';
                break;
        }
    }

    /**
     * HTML to be displayed after hours output
     * @param  array  $options Display options
     * @return string          HTML
     */
    private static function endHours($options)
    {
        switch ($options['display']) {
            case 'list':
                return '</ul>';
                break;
            case 'table':
                return '</table>';
                break;
            case 'div':
                return '</div>';
                break;
        }
    }
}
