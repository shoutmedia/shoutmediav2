<div class="<?php Layout::classes('related-projects'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partial('videobg'); ?>
    <div class="container">
        <?php if(Field::exists('title')) : ?>
            <div class="title-block">
                <?php Field::html('title', '<h2 class="title">%s</h2>'); ?>
            </div>
        <?php endif; ?>
        <div class="projects">
            <div class="project"><a href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-tbso.png" alt=""></a></div>
            <div class="project"><a href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-winmar.png" alt=""></a></div>
            <div class="project"><a href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-truegrit.png" alt=""></a></div>
            <div class="project"><a href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-aditya.png" alt=""></a></div>
        </div>
    </div>
</div>
