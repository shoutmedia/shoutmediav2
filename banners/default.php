<div class="banner" style="<?php Layout::partial('background'); ?>">
    <?php Layout::partial('videobg'); ?>
    <div class="container">
        <div class="summary">
            <?php Field::html('banner_title', '<h1>%s</h1>'); ?>
            <?php Field::display('banner_desc') ?>
        </div>
        <div class="accent">
            <?php Field::image('accent_image', [600, 600]); ?>
        </div>
    </div>
</div>
