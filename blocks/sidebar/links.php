<?php if(Field::exists('links')) : ?>
    <div class="links">
        <nav aria-label="page">
            <ul>
                <?php foreach(Field::get('links') as $link) : ?>
                    <?php if($link['type'] == 'heading') : ?>
                        <li class="heading"><?php echo $link['label']; ?></li>
                    <?php elseif($link['url']) : ?>
                        <li><a href="<?php echo check_url($link['url']); ?>"><?php echo $link['label']; ?></a></li>
                    <?php else : ?>
                        <li><?php echo $link['label']; ?></li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </nav>
    </div>
<?php endif; ?>
