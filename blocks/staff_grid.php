<div class="<?php Layout::classes('staff-grid'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partial('videobg'); ?>
    <div class="container">
        <div class="grid">
            <?php foreach(Field::iterable('active_staff') as $post) :
                setup_postdata($post); ?>
                <div class="staff">
                    <?php the_post_thumbnail(); ?>
                    <div class="slide-out">
                        <h5><?php the_title(); ?></h5>
                        <?php the_content(); ?>
                    </div>
                </div>
            <?php endforeach; ?>
            <?php wp_reset_postdata(); ?>
        </div>
    </div>
</div>
