<div class="<?php Layout::classes('project-grid'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partial('videobg'); ?>
    <div class="container">
        <?php if(Field::exists('items')) : ?>
            <div class="inner">
                <?php foreach(Field::iterable('items') as $item) : ?>
                    <div class="project">
                        <div class="details">
                            <?php Field::html('client', '<p class="client">%s</p>'); ?>
                            <?php Field::html('title', '<p class="name">%s</p>'); ?>
                            <?php Layout::partial('button'); ?>
                        </div>
                        <div class="accent">
                            <?php Field::image('image', 'medium'); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
