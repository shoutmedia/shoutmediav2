<?php get_header(); ?>

    <main class="main-content" role="main" id="main" tabindex="-1">

        <?php Banner::render(['type' => 'archives']); ?>

        <?php if (have_posts()) : ?>

            <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

            <h2><?php echo get_the_archive_title(); ?></h2>

            <?php while (have_posts()) : the_post(); ?>

                <article <?php post_class() ?>>

                    <h2 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

                    <?php printf( __( '<span class="sep">Posted </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a> by <span class="byline author vcard">%5$s</span>', '' ),
                        esc_url( get_permalink() ),
                        esc_attr( get_the_time() ),
                        esc_attr( get_the_date( 'c' ) ),
                        esc_html( get_the_date() ),
                        esc_attr( get_the_author() )
                    ); ?>

                    <div class="entry">
                        <?php the_content(); ?>
                    </div>

                </article>

            <?php endwhile; ?>

        <?php else : ?>

            <h2><?php _e('Nothing Found', DOMAIN); ?></h2>

        <?php endif; ?>

    </main>

<?php get_footer(); ?>
