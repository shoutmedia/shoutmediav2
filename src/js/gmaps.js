/* global google */
export default class GoogleMap {
    constructor(el, config = {}) {
        const args = Object.assign({
            zoom: 16,
            center: new google.maps.LatLng(0, 0),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
        }, config);

        // create map
        this.map = new google.maps.Map(el, args);

        // add a markers reference
        this.map.markers = [];

        return this;
    }
    addMarker(markerElement) {
        const latlng = new google.maps.LatLng(markerElement.getAttribute('data-lat'), markerElement.getAttribute('data-lng'));

        // create marker
        const marker = new google.maps.Marker({
            position: latlng,
            map: this.map,
        });

        // add to array
        this.map.markers.push(marker);

        // if marker contains HTML, add it to an infoWindow
        if (markerElement.textContent) {
            // create info window
            const infowindow = new google.maps.InfoWindow({
                content: markerElement.textContent,
            });

            // show info window when marker is clicked
            google.maps.event.addListener(marker, 'click', () => {
                infowindow.open(this.map, marker);
            });
        }
    }
    centerMap() {
        const bounds = new google.maps.LatLngBounds();

        // loop through all markers and create bounds
        this.map.markers.forEach((marker) => {
            const latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
            bounds.extend(latlng);
        });

        // only 1 marker?
        if (this.map.markers.length === 1) {
            // set center of map
            this.map.setCenter(bounds.getCenter());
            this.map.setZoom(16);
        } else {
            // fit to bounds
            this.map.fitBounds(bounds);
        }
    }
}
