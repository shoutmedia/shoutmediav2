<div class="<?php Layout::classes('gallery'); ?>" <?php Layout::id(); ?>>
    <div class="items">
        <?php foreach(Field::iterable('images') as $key => $img) : ?>
            <div class="item"<?php Field::html('background_colour', ' style="background-color: %s;"'); ?>>
                <div class="container half">
                    <?php Field::image('image', [600,600]); ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
