<?php get_header(); ?>

    <main class="main-content" role="main" id="main" tabindex="-1">

        <?php Banner::render(['type' => 'blog']); ?>

        <?php if (have_posts()) : ?>

            <?php while (have_posts()) :

                the_post();

                ?>

                <article <?php post_class() ?> id="post-<?php the_ID(); ?>">

                    <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

                    <?php printf( __( '<span class="sep">Posted </span><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s" pubdate>%4$s</time></a> by <span class="byline author vcard">%5$s</span>', '' ),
                        esc_url( get_permalink() ),
                        esc_attr( get_the_time() ),
                        esc_attr( get_the_date( 'c' ) ),
                        esc_html( get_the_date() ),
                        esc_attr( get_the_author() )
                    ); ?>

                    <div class="entry">
                        <?php the_content(); ?>
                    </div>

                    <footer class="postmetadata">
                        <?php _e('Posted in', DOMAIN); ?> <?php the_category(', ') ?>
                    </footer>

                </article>

            <?php endwhile; ?>

        <?php else : ?>

            <h2><?php _e('Nothing Found', DOMAIN); ?></h2>

        <?php endif; ?>

    </main>

<?php get_footer(); ?>
