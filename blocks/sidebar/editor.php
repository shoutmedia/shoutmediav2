<div class="box">
    <?php if(Field::exists('title')) : ?>
        <h2 class="title"><?php Field::display('title'); ?></h2>
    <?php endif; ?>
    <?php Field::display('content'); ?>
</div>
