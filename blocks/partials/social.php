<?php

$options = array_merge([
    'prefix' => '',
    'suffix' => '',
], $data);

?>
<?php if(get_field('social_accounts', 'option')) : ?>
    <?php foreach(get_field('social_accounts', 'option') as $social) : ?>
        <?php echo $options['prefix']; ?><a href="<?php echo $social['url']; ?>" target="_blank" rel="noopener"><em class="fa <?php echo $social['icon']; ?>" aria-hidden="true"></em></a><?php echo $options['suffix']; ?>
    <?php endforeach; ?>
<?php endif; ?>
