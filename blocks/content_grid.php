<?php

$per_row = Field::get('items_per_row', 3);

?>
<div class="<?php Layout::classes('content-grid'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partial('videobg'); ?>
    <div class="container">
        <?php if(Field::exists('items')) : ?>
            <div class="items">
                <?php foreach(Field::iterable('items') as $key => $item) :

                    if($key % $per_row == 0 && $key > 0) :
                        echo '</div><div class="items">';
                    endif;

                    ?>
                    <div class="item">
                        <?php Field::image('image', 'medium'); ?>
                        <?php Field::html('label', '<h6>%s</h6>'); ?>
                        <?php Field::display('description'); ?>
                        <?php Layout::partial('button'); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
