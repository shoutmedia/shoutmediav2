
    <footer class="footer" role="contentinfo">
        <div class="container">
            <div class="title-block">
                <h5 class="title">
                    <span class="line1">We don't bite</span>
                    <span class="line2">talk to us</span>
                </h5>
            </div>

            <div class="main-info">
                <div class="locations">
                    <?php foreach(get_field('locations', 'option') as $location) : ?>
                        <div itemscope itemtype="http://schema.org/LocalBusiness" class="address">
                            <h6 class="label"><?php echo $location['label']; ?></h6>
                            <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                <?php if($location['address']) : ?>
                                    <span itemprop="streetAddress"><?php echo $location['address']; ?></span><br>
                                <?php endif; ?>
                                <?php if($location['city']) : ?>
                                    <span itemprop="addressLocality"><?php echo $location['city']; ?></span>,
                                <?php endif; ?>
                                <?php if($location['province']) : ?>
                                    <span itemprop="addressRegion"><?php echo $location['province']; ?></span><br>
                                <?php endif; ?>
                                <?php if($location['postal_code']) : ?>
                                    <span itemprop="postalCode"><?php echo $location['postal_code']; ?></span>
                                <?php endif; ?>
                            </p>
                            <?php if($location['phone_numbers']) : ?>
                                <?php foreach($location['phone_numbers'] as $phone) : ?>
                                    <?php _e($phone['label'], DOMAIN); ?> <span itemprop="<?php echo $phone['type'] == 'faxNumber' ? $phone['type'] : 'telephone'; ?>"><?php echo $phone['number']; ?></span>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="social">
                    <h6 class="label">Follow Us</h6>
                    <div class="icons">
                        <?php Layout::partial('social'); ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div id="footer-credit" class="alt"></div>

    <?php wp_footer(); ?>

</body>
</html>
