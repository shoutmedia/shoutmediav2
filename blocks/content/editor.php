<?php

$col_size = 12 / count(Field::get('columns', ['']));

?>
<?php if(Field::exists('columns')) : ?>
    <div class="columns">
        <div class="row nested">
            <?php foreach(Field::iterable('columns') as $col) : ?>
                <div class="col w<?php echo $col_size; ?>">
                    <?php Field::html('title', '<h3 class="heading">%s</h3>'); ?>
                    <?php Field::display('content'); ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>
