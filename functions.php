<?php

define('BASE', dirname(__FILE__));
define('DOMAIN', 'arcadia');
define('GOOGLEAPI', 'AIzaSyDivzhX9fUCCqvaJWhbVXu-y1EDBCycwuU');

// Helper functions
require_once(BASE . '/inc/helpers.php');

// Autoloader
require_once(BASE . '/inc/autoloader.php');

// Load Arcadia
\Arcadia\Setup::init();

/**
 * Current Theme Setup
 * @return void
 */
function theme_setup()
{
    // Thumbnails
    add_theme_support('post-thumbnails');

    // Title Tag
    add_theme_support('title-tag');

    // Menus
    register_nav_menu('primary', __('Navigation Menu', DOMAIN));
    register_nav_menu('secondary', __('Footer Menu', DOMAIN));

    // When inserting an image don't link it
    update_option('image_default_link_type', 'none');

    // Remove Gallery Styling
    add_filter('use_default_gallery_style', '__return_false');

    // Additional Image Sizes
    add_image_size('banner', 1920, 1920);
}

add_action('after_setup_theme', 'theme_setup');

/**
 * Load theme specific assets
 * @return void
 */
function scripts_styles()
{
    // Load Stylesheets
    wp_enqueue_style(DOMAIN . '-slick', get_template_directory_uri() . '/slick/slick.css');
    wp_enqueue_style(DOMAIN . '-slick-thme', get_template_directory_uri() . '/slick/slick-theme.css');
    wp_enqueue_style(DOMAIN . '-style', get_stylesheet_uri());

    // Google Maps
    wp_register_script(DOMAIN . '-gmaps', 'https://maps.googleapis.com/maps/api/js?key=' . GOOGLEAPI, [], null, true);

    if (Layout::containsBlock('map')) {
        wp_enqueue_script(DOMAIN . '-gmaps');
    }

    // Load JavaScript
    wp_enqueue_script(DOMAIN . '-polyfill', 'https://cdn.polyfill.io/v2/polyfill.min.js', [], null);
    wp_enqueue_script(DOMAIN . '-slick', get_template_directory_uri() . '/slick/slick.min.js', ['jquery'], null, true);
    wp_enqueue_script(DOMAIN . '-main', get_template_directory_uri() . '/js/main.js', ['jquery'], null, true);
}

add_action('wp_enqueue_scripts', 'scripts_styles');

/**
 * Custom Editor Styles
 * @param  array $init TinyMCE Options
 * @return array       Updated TinyMCE Options
 */
function editor_style_options($init)
{
    $style_formats = [
        [
            'title' => 'Title',
            'block' => 'h2',
            'classes' => 'title',
            'wrapper' => false,
        ],
        [
            'title' => 'Large Paragraph',
            'block' => 'p',
            'classes' => 'large',
            'wrapper' => false,
        ],
        [
            'title' => 'Button',
            'block' => 'a',
            'classes' => 'btn',
            'wrapper' => false,
        ],
    ];

    $init['style_formats'] = json_encode($style_formats);

    return $init;
}

add_filter('tiny_mce_before_init', 'editor_style_options');

/**
 * ACF Maps Key
 * @return void
 */
function my_acf_init()
{
    acf_update_setting('google_api_key', GOOGLEAPI);
}

add_action('acf/init', 'my_acf_init');

/**
 * Post Types
 * @return void
 */
function create_additional_post_types()
{
    register_post_type('project', [
        'labels' => [
            'name' => __('Projects', DOMAIN),
            'singular_name' => __('Project', DOMAIN),
            'add_new_item' => __('Add New Project', DOMAIN),
            'edit_item' => __('Edit Project', DOMAIN),
            'new_item' => __('New Project', DOMAIN),
            'view_item' => __('View Project', DOMAIN),
            'search_items' => __('Search Projects', DOMAIN),
            'not_found' => __('No Projects found', DOMAIN),
            'not_found_in_trash' => __('No Projects found in trash', DOMAIN),
        ],
        'menu_position' => 55,
        'public' => true,
        'supports' => [
            'title',
            'revisions',
        ],
        'exclude_from_search' => true,
        'capability_type' => 'post',
        'taxonomies' => ['category'],
    ]);

    register_post_type('staff', [
        'labels' => [
            'name' => __('Staff Members', DOMAIN),
            'singular_name' => __('Staff Member', DOMAIN),
            'add_new_item' => __('Add New Staff Member', DOMAIN),
            'edit_item' => __('Edit Staff Member', DOMAIN),
            'new_item' => __('New Staff Member', DOMAIN),
            'view_item' => __('View Staff Member', DOMAIN),
            'search_items' => __('Search Staff Members', DOMAIN),
            'not_found' => __('No Staff Members found', DOMAIN),
            'not_found_in_trash' => __('No Staff Members found in trash', DOMAIN),
        ],
        'menu_position' => 54,
        'public' => true,
        'supports' => [
            'title',
            'revisions',
            'editor',
            'thumbnail',
        ],
        'exclude_from_search' => true,
        'capability_type' => 'post',
    ]);
}

add_action('init', 'create_additional_post_types');
