<?php

namespace Arcadia;

use Arcadia\Dev\Builder;
use Arcadia\Theme\PostTypes;
use Arcadia\Theme\PromoteToWidget;
use Arcadia\Theme\Search;
use Arcadia\Theme\Assets;
use DateTime;
use DateTimeZone;

/**
 * Setup
 */
class Setup
{
    /**
     * Timezone
     * @var string
     */
    private static $timezone = '';

    /**
     * Prepare Theme
     * @return void
     */
    public static function init()
    {
        // Promote to Widget
        PromoteToWidget::init();

        // Extend Search
        Search::init();

        // Adjust stylesheets across theme
        Assets::init();

        // Set System Timezone
        self::$timezone = new DateTimeZone(get_option('timezone_string'));

        // Autoloader
        spl_autoload_register([__CLASS__, 'resources']);

        // Set Login URL
        add_filter('login_headerurl', [__CLASS__, 'loginUrl']);

        if (is_admin()) {
            // Dashboard Widgets
            add_action('wp_dashboard_setup', [__CLASS__, 'dashboardWidgets']);

            // Disable Editing
            add_action('admin_init', [__CLASS__, 'disableEditors']);

            // Keep track of last update
            add_action('save_post', [__CLASS__, 'trackUpdate'], 999, 2);

            // Activate Styleselect
            add_filter('mce_buttons_2', [__CLASS__, 'activateStyleFormats']);

            // Update flexible layout titles
            add_filter('acf/fields/flexible_content/layout_title/key=field_57222a09e15e1', [__CLASS__, 'blockVisibility'], 10, 4);

            // Remove H1
            add_filter('tiny_mce_before_init', [__CLASS__, 'removeH1FromEditor']);

            // ACF Meta Box
            add_filter('acf/settings/remove_wp_meta_box', '__return_true');

            // Theme Options
            self::createOptionsPage();

            // Block builder
            Builder::init();
        }

        // Remove unused features
        add_action('init', [__CLASS__, 'removeFeatures']);

        // Set up post types
        PostTypes::init();
    }

    /**
     * PSR-4 Autoloader
     * @param  string  $class Which class to load
     * @return boolean
     */
    public static function resources($className)
    {
        $directories = [
            BASE . '/inc/'
        ];

        foreach ($directories as $directory) {
            if (file_exists($directory . $className . '.php')) {
                require ($directory . $className . '.php');
                return true;
            }
        }

        return false;
    }

    /**
     * Remove unused functionality
     * @return void
     */
    public static function removeFeatures()
    {
        // App functionality
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');

        // Remove Emoji Support
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('admin_print_styles', 'print_emoji_styles');

        // Disable Tags
        unregister_taxonomy_for_object_type('post_tag', 'post');

        // WordPress Generator
        remove_action('wp_head', 'wp_generator');
    }

    /**
     * Clean up Dashboard widgets
     * @return void
     */
    public static function dashboardWidgets()
    {
        // Quick Press
        remove_meta_box('dashboard_quick_press', 'dashboard', 'side');

        // Recent Drafts
        remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');

        // WordPress blog
        remove_meta_box('dashboard_primary', 'dashboard', 'side');

        // Yoast
        remove_meta_box('wpseo-dashboard-overview', 'dashboard', 'normal');

        // Wordfence (Untested)
        remove_meta_box('wordfence_activity_report_widget', 'dashboard', 'normal');

        // Support Widget
        add_meta_box('shout_support_dashboard', 'Support', [__CLASS__, 'supportWidget'], 'dashboard', 'side', 'high');
    }

    /**
     * Display Support Widget
     * @return void
     */
    public static function supportWidget()
    {
        include(get_template_directory() . '/views/support_widget.php');
    }

    /**
     * Theme Options
     * @return void
     */
    public static function createOptionsPage()
    {
        if (function_exists('acf_add_options_page')) {
            $parent = acf_add_options_page([
                'page_title' => 'Theme Settings',
                'menu_title' => 'Theme Settings',
                'redirect' => false,
                'autoload' => true,
                'position' => 58,
            ]);
        }
    }

    /**
     * Set Login URL
     * @return string Current Website
     */
    public static function loginUrl()
    {
        return home_url();
    }

    /**
     * Disable Editors
     * @return void
     */
    public static function disableEditors()
    {
        // Theme Editor
        remove_submenu_page('themes.php', 'theme-editor.php');

        // Plugin Editor
        remove_submenu_page('plugins.php', 'plugin-editor.php');
    }

    /**
     * Add timestamp to update file
     * @return mixed
     */
    public static function trackUpdate($post_id, $post)
    {
        if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE || $post->post_status == 'auto-draft')) {
            return $post_id;
        }

        $exceptions = [
            'attachment',
            'acf-field-group',
        ];

        $post_type = get_post_type($post_id);

        if (in_array($post_type, $exceptions) || wp_is_post_autosave($post_id)) {
            return $post_id;
        }

        file_put_contents(BASE . '/updated.txt', time());
    }

    /**
     * Activate style dropdown
     * @param  array $buttons TinyMCE buttons
     * @return array          Updated TinyMCE buttons
     */
    public static function activateStyleFormats($buttons)
    {
        array_unshift($buttons, 'styleselect');

        return $buttons;
    }

    /**
     * Display visibility alongside block name
     * @param  string  $title  The layout title text
     * @param  array   $field  The flexible content field settings
     * @param  array   $layout The current layout settings
     * @param  integer $index  The current layout index
     * @return string          Extended layout title
     */
    public static function blockVisibility($title, $field, $layout, $index)
    {
        $visibility = get_sub_field('visibility');
        $now = new DateTime('now', self::$timezone);

        $formatted = '<span class="block-title" data-title="' . $title . '">';

        if (get_sub_field('friendly_title')) {
            $formatted .= get_sub_field('friendly_title') . ' (' . $title . ')';
        } else {
            $formatted .= $title;
        }

        $formatted .= '</span>';
        $formatted .= '<span class="block-visibility';

        switch ($visibility) {
            case 'disable':
                $formatted .= ' disabled"> - <em>Disabled</em>';
                break;
            case 'schedule':
                $start = new DateTime(get_sub_field('visible_from'), self::$timezone);
                $end = new DateTime(get_sub_field('visible_until'), self::$timezone);
                $format = 'F j @ g:i A';

                if (get_sub_field('visible_until') && $end < $now) {
                    $formatted .= ' disabled"> - <em>Disabled ' . $end->format($format) . '</em>';
                } elseif (get_sub_field('visible_from') && $start > $now) {
                    $formatted .= ' scheduled"> - <em>Scheduled for ' . $start->format($format) . '</em>';
                } elseif (get_sub_field('visible_until')) {
                    $formatted .= ' scheduled"> - <em>Scheduled until ' . $end->format($format) . '</em>';
                } else {
                    $formatted .= '">';
                }

                break;
            default:
                $formatted .= '">';
                break;
        }

        $formatted .= '</span>';

        return $formatted;
    }

    /**
     * Remove H1 from the TinyMCE editor
     * @param  array $init Options
     * @return array       Options
     */
    public static function removeH1FromEditor($init)
    {
        $init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Address=address;Pre=pre';

        return $init;
    }
}
