<div class="<?php Layout::classes('timeline'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partial('videobg'); ?>
    <div class="container">
        <?php if(Field::anyExist('sub_title', 'title')) : ?>
            <div class="title-block">
                <?php Field::html('sub_title', '<p class="subtitle">%s</p>'); ?>
                <?php Field::html('title', '<h2 class="title">%s</h2>'); ?>
            </div>
        <?php endif; ?>
        <?php if(Field::exists('segments')) : ?>
            <?php foreach(Field::iterable('segments') as $key => $segment) : ?>
                <div class="segment<?php Field::displayIfEquals('accent_placement', 'left', ' left'); ?>">
                    <?php if(Field::exists('description') && !Field::equals('accent_placement', 'center')) : ?>
                        <div class="desc">
                            <div class="step">
                                <span><?php echo chr($key + 65); ?></span>
                            </div>
                            <div class="text">
                                <?php Field::display('description'); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if(Field::exists('accent') && !Field::equals('accent_placement', 'hidden')) : ?>
                        <div class="accent">
                            <?php Field::display('accent'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
