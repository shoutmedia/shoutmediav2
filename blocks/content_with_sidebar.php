<div class="<?php Layout::classes('content-with-sidebar'); ?>" style="<?php Layout::partial('background') ?>"<?php Layout::id(); ?>>
    <?php Layout::partial('videobg'); ?>
    <div class="container">
        <?php Field::html('section_title', '<h2 class="section-title">%s</h2>'); ?>

        <div class="inner">
            <div class="content">
                <?php Layout::flexible(Field::get('content_rows', []), 'blocks/content'); ?>
            </div>
            <div class="sidebar">
                <?php Layout::flexible(Field::get('side_rows', []), 'blocks/sidebar'); ?>
            </div>
        </div>
    </div>
</div>
