<?php

/**
 * Decide on text color based on background
 * @param  string $hexcolor Background Color
 * @param  string $dark     Dark contrast colour
 * @param  string $light    Light contrast colour
 * @return string           Color
 */
function get_contrast($hexcolor, $dark = 'black', $light = 'white')
{
    if (substr($hexcolor, 0, 1) == '#') {
        $hexcolor = substr($hexcolor, 1);
    }

    $r = hexdec(substr($hexcolor, 0, 2));
    $g = hexdec(substr($hexcolor, 2, 2));
    $b = hexdec(substr($hexcolor, 4, 2));

    $yiq = (($r * 299) + ($g * 587) + ($b * 114)) / 1000;

    return ($yiq >= 128) ? $dark : $light;
}

/**
 * Retrieve meta data about attachments
 * @param  integer $attachment_id Attachment ID
 * @param  array   $size          Image Size
 * @return array                  Details
 */
function wp_get_attachment($attachment_id, $size = [])
{
    $src        = wp_get_attachment_image_src($attachment_id, $size);
    $attachment = get_post($attachment_id);

    // Attachment doesn't exist
    if (!$src) {
        return false;
    }

    return [
        'alt'         => get_post_meta($attachment->ID, '_wp_attachment_image_alt', true),
        'caption'     => $attachment->post_excerpt,
        'description' => $attachment->post_content,
        'href'        => get_permalink($attachment->ID),
        'src'         => $src[0],
        'width'       => $src[1],
        'height'      => $src[2],
        'title'       => $attachment->post_title
    ];
}

/**
 * Take a sentence and split directly in half
 * @param  string $line Sentence to split in half
 * @return array        Results
 */
function split_line($line = '')
{
    $words   = explode(" ", $line);
    $length  = ceil(strlen($line) / 2);
    $lengths = array();

    // Just return 1 word strings
    if (count($words) < 1) {
        return array($line, '');
    }

    foreach ($words as $key => $word) {
        $prev = 0;

        if (array_key_exists($key - 1, $lengths)) {
            $prev = $lengths[$key - 1];
        }

        $lnlen = $prev + strlen($word);

        // Add 1 to account for spaces
        if ($key > 0) {
            $lnlen++;
        }

        $lengths[] = $lnlen;
    }

    $closest = null;

    foreach ($lengths as $item) {
        if ($closest == null || abs($length - $closest) > abs($item - $length)) {
            $closest = $item;
        }
    }

    $line1 = substr($line, 0, $closest);
    $line2 = substr($line, $closest + 1);

    return array($line1, $line2);
}

/**
 * Make sure HTTP has been added to URL
 * @param  string $url URL
 * @return string      Fixed URL
 */
function check_url($url)
{
    if (substr($url, 0, 1) != '#' && substr($url, 0, 4) != 'http') {
        return 'http://' . $url;
    }

    return $url;
}
