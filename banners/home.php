<div class="banner-home"<?php echo Field::exists('banner_background') ? ' style="background-image: url(' . Field::src('banner_background', 'banner') . ');"' : ''; ?>>
    <div class="container">
        <?php if(Field::exists('banner_title')) : ?>
            <h1><?php Field::display('banner_title'); ?></h1>
        <?php endif; ?>
        <?php Field::display('banner_desc') ?>
    </div>
</div>
