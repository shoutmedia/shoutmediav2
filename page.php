<?php get_header(); ?>

    <main class="main-content" role="main" id="main" tabindex="-1">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <?php Banner::render(); ?>
            <?php Layout::render(); ?>

        <?php endwhile; endif; ?>

    </main>

<?php get_footer(); ?>
