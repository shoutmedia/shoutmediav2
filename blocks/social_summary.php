<div class="<?php Layout::classes('social-summary'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partial('videobg'); ?>
    <div class="container">
        <div class="title-block">
            <?php Field::html('title', '<h2 class="title">%s</h2>'); ?>
        </div>

        <ul class="social">
            <?php if(get_field('social_accounts', 'option')) : ?>
                <?php foreach(get_field('social_accounts', 'option') as $social) : ?>
                    <li><a href="<?php echo $social['url']; ?>" target="_blank" rel="noopener"><em class="fa <?php echo $social['icon']; ?>" aria-hidden="true"></em><span><?php echo $social['label']; ?></span></a></li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</div>
