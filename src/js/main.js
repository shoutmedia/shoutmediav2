import GoogleMap from './gmaps';
import SmoothScroll from './smoothscroll';

/**
 * Activate smooth scroll
 */
const scroll = new SmoothScroll();

/**
 * Google maps
 */
const maps = document.querySelectorAll('.acf-map');

Array.from(maps).forEach((map) => {
    const markers = map.querySelectorAll('.marker');
    const gmap = new GoogleMap(map);

    Array.from(markers).forEach((marker) => {
        gmap.addMarker(marker);
    });

    gmap.centerMap();
});

/**
 * Add anchor link tracking
 */
const links = document.querySelectorAll('a[href*="#"]');

Array.from(links).forEach((element) => {
    let href = element.getAttribute('href');

    // Clean up URL
    if (href.indexOf('#') !== false) {
        href = href.substr(href.indexOf('#'));
    }

    if (href.length > 2) {
        element.addEventListener('click', () => {
            // Make sure the anchor is on the current page
            if (location.pathname.replace(/^\//, '') === element.pathname.replace(/^\//, '')
                && location.hostname === element.hostname) {
                scroll.to(href);
            }
        });
    }
});

/**
 * Check URL for hash
 */
window.addEventListener('load', () => {
    if (window.location.hash) {
        scroll.to(window.location.hash);
    }
});

jQuery(($) => {
    function caseSlide() {
        $(this).find('.slideshow').slick({
            infinite: true,
            arrows: true,
            appendArrows: $(this).find('.slide-arrows .container'),
        });
    }

    /**
     * Blog Feed
     */
    if ($('.blog-feed .posts .post').length > 0) {
        $('.blog-feed .posts').slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
        });
    }

    /**
     * Accent Image Slideshow
     */
    if ($('.accent-image .slideshow .slide').length > 0) {
        $('.accent-image .slideshow').slick({
            infinte: true,
            arrows: true,
        });
    }

    /**
     * Case Studies
     */
    if ($('.case-studies').length > 0) {
        $('.case-studies').each(caseSlide);
    }
});

/**
 * Video
 */
const videoControls = document.querySelectorAll('.video-controls button');

if (videoControls) {
    Array.from(videoControls).forEach((target) => {
        const parent = target.closest('.video');
        const video = parent.querySelector('video');
        const icon = target.querySelector('.fa');

        target.addEventListener('click', () => {
            if (video.paused) {
                // Play the video
                video.play();

                // Update the icon to 'Pause'
                icon.classList.add('fa-pause');
                icon.classList.remove('fa-play');
            } else {
                // Pause the video
                video.pause();

                // Update the icon to 'Play'
                icon.classList.remove('fa-pause');
                icon.classList.add('fa-play');
            }
        });
    });
}
