<div class="<?php Layout::classes('accent-image'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partials('videobg', 'overlay'); ?>
    <div class="container">
        <div class="accent">
            <?php if(Field::equals('accent_type', 'single')) : ?>
                <?php Field::image('accent_image', [465, 465]); ?>
            <?php else : ?>
                <div class="slideshow">
                    <?php foreach(Field::iterable('accent_slides') as $slide) : ?>
                        <div class="slide"><?php Field::image('image'); ?></div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="content">
            <?php if(Field::exists('title')) : ?>
                <div class="title-block">
                    <?php Field::html('title', '<h2 class="title">%s</h2>'); ?>
                </div>
            <?php endif; ?>
            <?php Field::display('content'); ?>
            <?php Layout::partial('buttons'); ?>
        </div>
    </div>
</div>
