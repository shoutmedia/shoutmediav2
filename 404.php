<?php get_header(); ?>

    <main class="main-content" role="main" id="main" tabindex="-1">
        <?php Banner::render(['type' => 'not_found']); ?>
        <div class="page-not-found">
            <div class="container">
                <h2><?php _e('Page Not Found', DOMAIN); ?></h2>
                <p><?php _e('The page you are looking for can\'t be found.', DOMAIN); ?></p>
            </div>
        </div>
    </main>

<?php get_footer(); ?>
