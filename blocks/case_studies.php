<div class="<?php Layout::classes('case-studies'); ?>"<?php Layout::id(); ?>>
    <div class="slide-arrows">
        <div class="container"></div>
    </div>
    <div class="slideshow">
        <?php foreach(Field::iterable('case_studies') as $study) :

            $styles = [];

            if (Field::exists('background_image')) {
                $styles[] = 'background-image: url(' . Field::src('background_image', 'banner') . ')';
            }

            if (Field::exists('background_colour')) {
                $styles[] = 'background-color: ' . Field::get('background_colour');
            }

            ?>
            <div class="study"<?php echo !empty($styles) ? ' style="' . implode(";", $styles) . '"' : ''; ?>>
                <div class="container">
                    <?php if(Field::exists('accent')) : ?>
                        <div class="accent">
                            <?php Field::image('accent'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="desc">
                        <?php Field::html('sub_title', '<h4 class="subtitle">%s</h4>'); ?>
                        <?php Field::html('title', '<h3 class="title">%s</h3>'); ?>
                        <?php Field::html('description', '<p>%s</p>'); ?>
                        <?php Layout::partial('button'); ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
