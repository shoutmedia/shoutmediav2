<div class="<?php Layout::classes('video'); ?>"<?php Layout::id(); ?>>
    <video poster="data:image/gif,AAAA"<?php echo Field::exists('poster') ? ' style="background-image: url(' . Field::src('poster', 'large') . ');"' : ''; ?>>
        <?php Field::html('video_webm', '<source src="%s" type="video/webm">'); ?>
        <?php Field::html('video_mp4', '<source src="%s" type="video/mp4">'); ?>
    </video>
    <div class="video-controls">
        <button type="button"><em class="fa fa-play"></em></button>
    </div>
</div>
