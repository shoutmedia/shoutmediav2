<div class="<?php Layout::classes('blog-feed'); ?>" style="<?php Layout::partial('background'); ?>"<?php Layout::id(); ?>>
    <?php Layout::partial('videobg'); ?>
    <div class="container">
        <div class="preview">
            <?php Field::html('title', '<h2 class="title">%s</h2>'); ?>
            <?php Field::display('description'); ?>
        </div>
        <div class="posts">
            <div class="post" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/blog1.jpg);">
                <div class="summary">
                    <time>05.12.2017</time>
                    <h5 class="headline">Suzanne's Pancake Recipe</h5>
                    <a href="#" class="btn read"><span>Read</span><em class="fa fa-caret-right"></em></a>
                </div>
            </div>
            <div class="post" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/blog2.jpg);">
                <div class="summary">
                    <time>05.12.2017</time>
                    <h5 class="headline">Taylor's Hockey Ramblings</h5>
                    <a href="#" class="btn read"><span>Read</span><em class="fa fa-caret-right"></em></a>
                </div>
            </div>
        </div>
    </div>
</div>
