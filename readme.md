# Arcadia

Current Version: `3.1.x`

## Requirements

* [PHP 5.6+](http://php.net)
* [Node](http://nodejs.com)
* [FontForge](https://fontforge.github.io/en-US/)
* [AdvancedCustomFields 5.4.7+](https://www.advancedcustomfields.com/)

## Prerequisites

It is required that the following packages be installed globally:

```
npm install -g gulp@^3.9.1 eslint-config-airbnb@^13.0.0 eslint@^3.9.1 eslint-plugin-jsx-a11y@^2.2.3 eslint-plugin-import@^2.1.0 eslint-plugin-react@^6.6.0
```

## Getting started

1. First you will need to install some packages using the following `npm install -g gulp browser-sync && npm install`
2. Update `src/scss/style.scss` with your project details.
3. Update `gulpfile.js` setting `url` to the root of your local WordPress install.
4. Update `define('DOMAIN', 'arcadia');` within `functions.php` to reflect the name of your project.
5. Windows users refer to section below.
6. And finally run `gulp`

## Windows Users

1. You will need FontForge added to your system PATH.

## Sublime Users

1. If you experience issues with partial SCSS files being unreadable. Add "atomic_save": true to your preferences. It will increase SCSS compilation times but will resolve the issue.

## Notes

1. Update Google Maps API keys

## Features

* [FontAwesome](http://fortawesome.github.io/Font-Awesome/)
* [SASS](http://sass-lang.com)
* [jQuery](http://jquery.com)
* [Gulp](http://gulpjs.com)
* [ESLint](http://esling.org)
