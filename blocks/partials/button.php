<?php

if(Field::exists('btn_label') && Field::anyExist('btn_resource', 'btn_url', 'btn_anchor')) :
    switch(Field::get('btn_type')) :
        case 'internal':
            $url = Field::get('btn_resource');
            break;
        case 'external':
            $url = Field::url('btn_url');
            break;
        case 'relative':
            $url = '#' . Field::get('btn_anchor');
            break;
        case 'media':
            $url = Field::get('btn_file');
            break;
    endswitch;

    $label = '<span class="btn-label">' . Field::get('btn_label') . '</span>';

    if(Field::exists('add_btn_icon')) :
        if(Field::equals('btn_icon_placement', 'before')) :
            $label = Field::get('btn_icon') . $label;
        else :
            $label .= Field::get('btn_icon');
        endif;
    endif;

    ?>
    <a href="<?php echo $url; ?>"<?php Field::displayIfEquals('btn_target', '_blank', ' target="%s"'); ?> class="btn"<?php Field::displayIfEquals('btn_type', 'external', ' rel="external noopener"'); ?>><?php echo $label; ?></a>
<?php endif; ?>
